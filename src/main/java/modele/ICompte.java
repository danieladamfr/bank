package modele;

public interface ICompte {

	public void crediterCompte(double montant);
	public void debiterCompte(double montant);
	public double getSolde();
	public int getIdCompte();
}
