package dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import modele.Client;
import modele.IClient;
import modele.ICompte;

public class BasicClientDAO  extends AbstractDAOObject implements ClientDAO{

	private static BasicClientDAO monInstance;
	private BasicClientDAO() {
		super();
	}
	
	
	public static BasicClientDAO getInstance(){
		if (monInstance ==null){
			monInstance = new BasicClientDAO();
			System.out.println(monInstance.getBd());
		}
		return monInstance;
		
	}
	
	public IClient addClient(String nom, String prenom, String adresse, String ville) {
		// TODO Auto-generated method stub
		
		String req = "INSERT INTO MesBanques.Client(nomClient,prenomClient,adresseClient,villeClient) VALUES(' "+nom+"','"+prenom+"','"+adresse+"','"+ville+"');";
		this.getBd().open();
		ResultSet res = null;
		Integer id = null;
		try {
			res = this.getBd().executeUpdate(req);
			System.out.println(res);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			if (res!=null && res.next()){
				id = res.getInt(1);
			}
			
			if (id!= null) {
				return new Client(id,nom,prenom,adresse,ville);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	public IClient getClient(int idClient) {
		// TODO Auto-generated method stub
		String req = "SELECT * FROM MesBanques.Client WHERE MesBanques.Client.idClient="+idClient;
		this.getBd().open();
		try {
			ResultSet res = this.getBd().executeRequest(req);
			if (res != null && res.next()){
				int id = res.getInt("idClient");
				String nom = res.getString("nomClient");
				String prenom = res.getString("prenomClient");
				String adresse = res.getString("adresseClient");
				String ville = res.getString("villeClient");
				String reqIdCompte = "SELECT idCompte FROM MesBanques.AUnCompte WHERE MesBanques.AUnCompte.idClient="+idClient;
				ResultSet resIdCompte = this.getBd().executeRequest(reqIdCompte);
				ICompte c=null;
				if (resIdCompte.next()){
					int idCompte = resIdCompte.getInt("idCompte");
					c = BasicCompteDAO.getInstance().getCompte(idCompte);
				}
				this.getBd().close();
				IClient monClient = new Client(id,nom,prenom,adresse,ville);
				monClient.setCompteClient(c);
				return monClient;
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			this.getBd().close();
		}
		
		return null;
	}

}
