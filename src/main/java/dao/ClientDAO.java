package dao;

import modele.IClient;

public interface ClientDAO {

	
	public IClient addClient(String nom, String prenom, String adresse, String ville);
	
	public IClient getClient(int idClient);
}
