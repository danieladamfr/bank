package dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import modele.Client;
import modele.Compte;
import modele.ICompte;

public class BasicCompteDAO extends AbstractDAOObject implements CompteDAO {

	
	private static BasicCompteDAO monInstance;
	
	private BasicCompteDAO(){
		super();
	}
	
	
	public static BasicCompteDAO getInstance(){
		if (monInstance == null) {
			monInstance = new BasicCompteDAO();
		}
		return monInstance;
	}
	
	public ICompte creerCompte(int solde) {
		// TODO Auto-generated method stub
		String req = "INSERT INTO MesBanques.Compte(solde) VALUES("+solde+");";
		this.getBd().open();
		ResultSet res = null;
		Integer id = null;
		try {
			res = this.getBd().executeUpdate(req);
			System.out.println(res);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			if (res!=null && res.next()){
				id = res.getInt(1);
			}
			
			if (id!= null) {
				return new Compte(id,solde);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	public ICompte getCompte(int idCompte) {
		// TODO Auto-generated method stub
		String reqClients = "SELECT solde FROM MesBanques.Compte WHERE idCompte="+idCompte;
		
		try {
			this.getBd().open();
			ResultSet res = this.getBd().executeRequest(reqClients);
			if (res!= null && res.next()){
			double solde = res.getDouble("solde");
			ICompte cp = new Compte(solde,idCompte);
			this.getBd().close();
			return cp;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public ICompte majCompte(int idCompte, double montant) {
		// TODO Auto-generated method stub
		return null;
	}


	public void suppressionCompte(ICompte c) {
		// TODO Auto-generated method stub
		
	}

}
