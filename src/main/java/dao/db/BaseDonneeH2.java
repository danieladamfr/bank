package dao.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.h2.jdbcx.JdbcDataSource;

public class BaseDonneeH2 implements IBaseDonnee {
	 private JdbcDataSource ds;
	 private Connection conn;
	 private Statement stmt;
	 private String dbPath = "src/db/banque";
	 private static BaseDonneeH2 monInstance;
	 private BaseDonneeH2() {
		 try {                   
			 Class.forName("org.h2.Driver");
			 ds = new JdbcDataSource();
			 ds.setURL("jdbc:h2:"+ dbPath);
			 ds.setUser("admin");
			 ds.setPassword("admin");
	            
     	} catch (ClassNotFoundException e) {
     		System.err.println("cannot create datasource");
     		e.printStackTrace();
    	}
	 }
	 
	 
	 
	 public static IBaseDonnee getInstance() {
		 if (monInstance == null) {
			 monInstance = new BaseDonneeH2();
			monInstance.open();
			 monInstance.createSchema();
			 monInstance.close();
		 }
		 return monInstance;
	 }
	 
	 	/* (non-Javadoc)
		 * @see dao.db.IBaseDonnee#open()
		 */
	 	public void open(){
	 		 try {
					this.conn = this.ds.getConnection();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				 try {
					this.stmt = this.conn.createStatement();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	 	}
	 
	 
	    /* (non-Javadoc)
		 * @see dao.db.IBaseDonnee#close()
		 */
	    public void close(){
	        try {
	        	if(stmt != null)
	            stmt.close();
	        	if(conn != null)
	            conn.close();
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	        
	    }
	    	    
	    
	    private void createSchema() {
	    	
	    	
	    	String query = "CREATE SCHEMA IF NOT EXISTS MesBanques;"
	    					+ "CREATE TABLE IF NOT EXISTS MesBanques.Client (idClient INT NOT NULL AUTO_INCREMENT, nomClient VARCHAR2(50), prenomClient VARCHAR2(50), adresseClient VARCHAR2(50), villeClient VARCHAR2(50), PRIMARY KEY(idClient));"
	    					+ "CREATE TABLE IF NOT EXISTS MesBanques.Compte (idCompte INT NOT NULL AUTO_INCREMENT, solde DOUBLE NOT NULL, PRIMARY KEY(idCompte));"
	    					+ "CREATE TABLE IF NOT EXISTS MesBanques.AUnCompte (idClient INT NOT NULL,  idCompte INT NOT NULL, PRIMARY KEY(idClient, idCompte), FOREIGN KEY(idClient) REFERENCES MesBanques.Client(idClient) ON DELETE CASCADE, FOREIGN KEY(idCompte) REFERENCES MesBanques.Compte(idCompte) ON DELETE CASCADE);";
	    	
	    	try {
	            stmt = conn.createStatement();
	    	
	    	stmt.execute(query);
	    	
	    	
	    	} catch(SQLException e) {
	            e.printStackTrace();
	        }	
		}

	    
		/* (non-Javadoc)
		 * @see dao.db.IBaseDonnee#executeRequest(java.lang.String)
		 */
		public ResultSet executeRequest(String sql) throws SQLException{
	        return stmt.executeQuery(sql);
	        
	        
	    }
	    
	    
	    /* (non-Javadoc)
		 * @see dao.db.IBaseDonnee#executeUpdate(java.lang.String, int)
		 */
	    public ResultSet executeUpdate(String sql) throws SQLException{
	         stmt.executeUpdate(sql);
	         return stmt.getGeneratedKeys();
	        
	    }

		/* (non-Javadoc)
		 * @see dao.db.IBaseDonnee#getDbPath()
		 */
		public String getDbPath() {
			return dbPath;
		}

		/* (non-Javadoc)
		 * @see dao.db.IBaseDonnee#setDbPath(java.lang.String)
		 */
		public void setDbPath(String dbPath) {
			this.dbPath = dbPath;
		}



	    
	 
	 
}
