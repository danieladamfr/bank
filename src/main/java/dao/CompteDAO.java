package dao;

import modele.ICompte;

public interface CompteDAO {

	public ICompte creerCompte(int solde);
	public ICompte getCompte(int idCompte);
	public ICompte majCompte(int idCompte, double montant);
	public void suppressionCompte(ICompte c);
}
